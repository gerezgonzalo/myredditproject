plugins {
    id(Plugins.android)
    id(Plugins.kotlinAndroid)
    id(Plugins.kotlinAndroidExt)
    id(Plugins.kotlinKapt)
    id(Plugins.hilt)
}

android {
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)

    defaultConfig {
        applicationId = AppConfig.applicatoinId
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner = AppConfig.androidTestInstrumentation
    }

    buildTypes {

        getByName("debug") {
            buildConfigField("String", "BASE_URL", "\"https://www.reddit.com\"")
        }

        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        freeCompilerArgs = AppConfig.compileArgs
        jvmTarget = "1.8"
    }

    androidExtensions {
        isExperimental = true
        features = AppConfig.features
    }
}

dependencies {

    implementation(project(":core"))
    implementation(Dependencies.kotlinStdlib)
    implementation(Dependencies.AndroidX.coreKtx)
    implementation(Dependencies.AndroidX.androidCompatibility)
    implementation(Dependencies.Google.material)
    implementation(Dependencies.AndroidX.constraintLayout)
    implementation(Dependencies.AndroidX.recyclerView)
    implementation(Dependencies.AndroidX.lifeCicleExt)
    implementation(Dependencies.AndroidX.archViewModel)
    implementation(Dependencies.AndroidX.legacySupport)
    implementation(Dependencies.AndroidX.lifecycleCompiler)
    implementation(Dependencies.AndroidX.lifecycleLivedataKtx)
    implementation(Dependencies.AndroidX.viewModelExt)
    implementation(Dependencies.AndroidX.activityExt)
    implementation(Dependencies.AndroidX.fragmentExt)
    kapt(Dependencies.AndroidX.hiltCompiler)
    implementation(Dependencies.AndroidX.hiltLifecycleViewModel)
    implementation(Dependencies.Google.gson)
    implementation(Dependencies.Google.hilt)
    kapt(Dependencies.Google.hiltKpt)
    implementation(Dependencies.gsonConverter)
    implementation(Dependencies.callAdapter)
    implementation(Dependencies.retrofit)
    implementation(Dependencies.logginInterceptor)
    implementation(Dependencies.kotlinxCoroutines)
    implementation(Dependencies.Fresco.fresco)

    testImplementation(Dependencies.Test.androidArch)
    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.mockitoCore)
    testImplementation(Dependencies.Test.kotlinCoroutines)
    testImplementation(Dependencies.Test.mockk)
    testImplementation(Dependencies.Test.robolectriCore)
    testImplementation(Dependencies.Test.robolectric)
    androidTestImplementation(Dependencies.Test.mockkInstrumented)
}