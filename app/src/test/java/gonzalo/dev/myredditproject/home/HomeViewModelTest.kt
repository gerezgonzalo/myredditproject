package gonzalo.dev.myredditproject.home

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import gonzalo.dev.core.data.dto.Children
import gonzalo.dev.core.usecase.GetTopPosts
import gonzalo.dev.myredditproject.common.AssetsUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    companion object {
        private const val TAG = "HomeViewMTest"
    }

    /**
     * Rule to allow to android components used in our unit tests to be executed synchronously.
     */
    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: HomeViewModel

    @Mock
    lateinit var getTopPosts: GetTopPosts

    @Mock
    lateinit var appContext: Application

    @Mock
    lateinit var observer: Observer<ArrayList<Children>>

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        MockitoAnnotations.initMocks(this)
        viewModel = HomeViewModel(appContext, getTopPosts)

    }

    @Test
    fun testFetchSuccess() {
        runBlockingTest {
            //Do mock
            val topResponse = AssetsUtils.loadAssetAsString("topResponse.json")
            //val responseObj = GsonUtils.getGson().fromJson(topResponse, TopResponse::class.java)

            val flowTopResponse = getTopPosts.invoke()

            /* given { runBlocking { getTopPosts.invoke() } }.willReturn(flowTopResponse)

            //Verify
             viewModel.postsState.observeForever{
                 it.size shouldEqualTo 2
             }*/

            //Execute
            viewModel.readPosts()
        }
    }

}
