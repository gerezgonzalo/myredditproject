package gonzalo.dev.myredditproject.common

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

@RunWith(RobolectricTestRunner::class)
abstract class AbstractRobolectricTest {

    fun getAppContext(): Context = ApplicationProvider.getApplicationContext()

    fun getApplication(): Application = getAppContext() as Application
}