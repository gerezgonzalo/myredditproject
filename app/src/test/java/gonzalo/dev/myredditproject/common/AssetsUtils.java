package gonzalo.dev.myredditproject.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

@SuppressWarnings({"PMD.AvoidFileStream"})
public final class AssetsUtils {

    private AssetsUtils() {
        // Avoid instances.
    }

    @SuppressWarnings("PMD.AvoidPrintStackTrace")
    public static String loadAssetAsString(final String fileName) {
        String assetString = null;

        try {
            assetString = tryToLoadAssetAsString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (assetString == null) {
            throw new IllegalArgumentException("Asset doesn't exist, fileName: " + fileName);
        }

        return assetString;
    }

    private static String tryToLoadAssetAsString(final String fileName) throws IOException {

        final ClassLoader classLoader = AssetsUtils.class.getClassLoader();
        final URL resource = classLoader.getResource(fileName);
        final FileInputStream fis = new FileInputStream(resource.getPath());

        final byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        fis.close();
        return new String(buffer, "UTF-8");
    }
}
