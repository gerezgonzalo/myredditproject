package gonzalo.dev.myredditproject.common;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Used to json operations.
 */
public final class GsonUtils {

    private GsonUtils() {
        // Avoid instances.
    }

    public static Gson getGson() {
        return new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();
    }
}
