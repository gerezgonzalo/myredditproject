package gonzalo.dev.myredditproject.home

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import dagger.hilt.android.AndroidEntryPoint
import gonzalo.dev.myredditproject.R
import gonzalo.dev.myredditproject.common.extensions.addFragment
import gonzalo.dev.myredditproject.common.extensions.replaceFragment
import gonzalo.dev.myredditproject.common.extensions.showFromStack
import gonzalo.dev.myredditproject.common.mvvm.BaseActivity
import gonzalo.dev.myredditproject.databinding.ActivityHomeBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeActivity : BaseActivity<HomeViewModel>() {

    private val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getViewModel().isDualPane = binding.secondPane?.visibility == View.VISIBLE
        if (getViewModel().isDualPane) {
            showFromStack(DetailFragment::class.java.simpleName)
                ?: addFragment(DetailFragment.newInstance(), R.id.secondPane)
        }
        showFromStack(PostListFragment::class.java.simpleName)
            ?: addFragment(PostListFragment.newInstance(), R.id.firstPane)

        getViewModel().fragmentDetailState.observe(this, {
            replaceFragment(DetailFragment.newInstance(it), R.id.firstPane, true)
        })
    }

    override fun createViewModelFactory(): HomeViewModel {
        val viewModel: HomeViewModel by viewModels()
        return viewModel
    }


    override fun getRootView(): View {
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        val searchView = menu?.findItem(R.id.home_search)?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                getViewModel().queryTextChange(newText)
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                //Do nothing.
                return false
            }
        })
        return true
    }
}