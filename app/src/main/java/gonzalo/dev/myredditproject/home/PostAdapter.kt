package gonzalo.dev.myredditproject.home

import android.text.format.DateUtils
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import gonzalo.dev.core.data.dto.Children
import gonzalo.dev.core.domain.model.Post
import gonzalo.dev.myredditproject.R
import gonzalo.dev.myredditproject.common.list.BaseAdapter
import gonzalo.dev.myredditproject.common.list.ICardClick
import gonzalo.dev.myredditproject.common.util.FrescoUtils

class PostAdapter(private val callback: ICardClick<Post>) :
    BaseAdapter<Children, PostAdapter.PostViewHolder>() {

    private var selectedPosition = RecyclerView.NO_POSITION

    override fun setData(dataSet: ArrayList<Children>) {
        _data = dataSet
        notifyDataSetChanged()
    }

    override fun getItemResource(): Int {
        return R.layout.item_list_post
    }

    override fun getViewHolderInstance(view: View): PostViewHolder {
        return PostViewHolder(view)
    }

    override fun onBind(holder: PostViewHolder, position: Int) {
        holder.item.isSelected = selectedPosition == holder.adapterPosition
        holder.bind(_data[holder.adapterPosition].data)
    }

    override fun doPerformFiltering(
        constraint: CharSequence?, originalDataSet: ArrayList<Children>
    ): FilterObject? {
        val filteredArray = ArrayList<Children>()
        for (children in originalDataSet) {
            if (children.data.contains(constraint.toString())) {
                filteredArray.add(children)
            }
        }
        return FilterObject(filteredArray, filteredArray.size)
    }

    inner class PostViewHolder(val item: View) : RecyclerView.ViewHolder(item) {

        /** Context for resources **/
        private val context = item.context

        private val title: TextView by lazy { item.findViewById(R.id.itemTitle) }
        private val entryDate: TextView by lazy { item.findViewById(R.id.itemEntryDate) }
        private val body: TextView by lazy { item.findViewById(R.id.itemBodyText) }
        private val comments: TextView by lazy { item.findViewById(R.id.itemComments) }
        private val thumb: SimpleDraweeView by lazy { item.findViewById(R.id.itemThumb) }
        private val dismiss: Button by lazy { item.findViewById(R.id.itemDismiss) }
        private val viewIndicator: ImageView by lazy { item.findViewById(R.id.viewIdicator) }

        fun bind(post: Post) {
            title.text = post.author
            entryDate.text = DateUtils.getRelativeTimeSpanString(post.createdUtc)
            body.text = post.title
            FrescoUtils.setImage(thumb, post.thumbnail)
            comments.text = context.getString(
                R.string.int_string, post.numComments,
                context.getString(R.string.comments)
            )

            if (post.clicked) viewIndicator.visibility = View.GONE else viewIndicator.visibility =
                View.VISIBLE

            dismiss.setOnClickListener {
                item.startAnimation(
                    AnimationUtils
                        .loadAnimation(context, R.anim.design_trans_slide_out_to_left)
                )

                if (adapterPosition != RecyclerView.NO_POSITION) {
                    _data.removeAt(adapterPosition)
                    notifyItemRemoved(adapterPosition)
                }
            }

            item.setOnClickListener {
                post.clicked = true
                viewIndicator.visibility = View.GONE
                notifyItemChanged(selectedPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition)
                callback.onItemClicked(post)
            }

            thumb.setOnClickListener {
                callback.onItemAction(post, thumb)
            }
        }
    }

}
