package gonzalo.dev.myredditproject.home

import android.app.Application
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import gonzalo.dev.core.data.dto.Children
import gonzalo.dev.core.data.dto.FeedData
import gonzalo.dev.core.domain.model.Post
import gonzalo.dev.core.usecase.GetTopPosts
import gonzalo.dev.myredditproject.common.SingleLiveEvent
import gonzalo.dev.myredditproject.common.mvvm.BaseViewModel
import gonzalo.dev.myredditproject.common.util.ErrorUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    val app: Application, private val getTopPosts: GetTopPosts
) : BaseViewModel(app) {

    companion object {
        private val TAG = "fataHomeViewModel"
    }

    lateinit var feedData: FeedData
    val dataSet = ArrayList<Children>()
    var isDualPane = false

    private val _postsState = MutableLiveData<ArrayList<Children>>()
    val postsState: LiveData<ArrayList<Children>> = _postsState

    private val _postsDetailState = SingleLiveEvent<Post>()
    val postsDetailState = _postsDetailState

    private val _fragmentDetailState = SingleLiveEvent<Post>()
    val fragmentDetailState = _fragmentDetailState

    private val _notifyState = MutableLiveData<Void>()
    val notifyState: LiveData<Void> = _notifyState

    private val _queryTextState = MutableLiveData<String>()
    val queryTextState: LiveData<String> = _queryTextState

    /**
     * Read all limited posts the first time.
     */
    fun readPosts() {
        if (dataSet.isNullOrEmpty()) {
            setViewStateAsLoading()
            doRequest()
        }
    }

    /**
     * Refresh the data set with the pull down gesture.
     */
    fun refreshList() {
        Log.i(TAG, "refreshList: ")
        dataSet.clear()
        doRequest()
    }

    /**
     * Load more items when the scroll was finalized.
     * @param nextPage The next page data to be fetched.
     */
    fun loadMore(nextPage: String) {
        Log.i(TAG, "loadMore: ")
        doRequest(nextPage)
    }

    @ExperimentalCoroutinesApi
    private fun doRequest(nextPage: String? = null) {
        Log.i(TAG, "readPosts: ")
        viewModelScope.launch {
            getTopPosts(nextPage)
                .catch {
                    setViewStateAsLayout()
                    errorState.postValue(ErrorUtils.errorMessage(app, it))
                }
                .collect {
                    setViewStateAsLayout()
                    feedData = it.data
                    nextPage?.let {
                        dataSet.addAll(feedData.children)
                        _notifyState.postValue(null)
                    } ?: dataSet.apply {
                        clear()
                        addAll(feedData.children)
                        _postsState.postValue(this)
                    }
                }
        }
    }

    /**
     * Show the post's detail in a new fragment or in the second pane depending.
     * @param item The selected post.
     */
    fun showPostDetail(item: Post) {
        Log.i(TAG, "showPostDetail: ")
        if (isDualPane) _postsDetailState.postValue(item) else _fragmentDetailState.postValue(item)
    }

    /**
     * Emit the value read from the search view.
     * @param value The string searched.
     */
    fun queryTextChange(value: String?) {
        value?.let {
            _queryTextState.postValue(value)
        }
    }


}