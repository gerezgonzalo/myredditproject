package gonzalo.dev.myredditproject.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import gonzalo.dev.core.domain.model.Post
import gonzalo.dev.myredditproject.common.util.FrescoUtils
import gonzalo.dev.myredditproject.databinding.FragmentPostDetailBinding

class DetailFragment : Fragment() {

    private val viewModel: HomeViewModel by activityViewModels()
    private val binding by lazy { FragmentPostDetailBinding.inflate(layoutInflater) }

    private var currentPost: Post? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            currentPost = it.getSerializable(EXTRA) as Post
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.postsDetailState.observe(viewLifecycleOwner, {
            currentPost = it
            updateUi(currentPost)
        })

        updateUi(currentPost)
    }

    private fun updateUi(currentPost: Post?) {
        currentPost?.let {
            binding.detailPrimaryText.text = currentPost.author
            FrescoUtils.setImage(binding.detailThumb, currentPost.thumbnail)
            binding.detailBody.text = currentPost.title
        }
    }

    companion object {
        private const val EXTRA = "df_extra"

        fun newInstance(post: Post? = null) = DetailFragment().apply {
            post?.let {
                arguments = Bundle().apply { putSerializable(EXTRA, it) }
            }
        }
    }
}