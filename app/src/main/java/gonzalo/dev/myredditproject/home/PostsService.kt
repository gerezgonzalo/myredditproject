package gonzalo.dev.myredditproject.home

import gonzalo.dev.core.data.dto.TopResponse
import gonzalo.dev.myredditproject.SessionManager
import gonzalo.dev.myredditproject.common.Timeout
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

@Timeout(10, TimeUnit.SECONDS)
interface PostsService {

    @GET("/top.json")
    suspend fun fetchPosts(
        @Query("after") nextPage: String,
        @Query("limit") limit: Int = SessionManager.DEFAULT_LIMIT
    ): TopResponse
}