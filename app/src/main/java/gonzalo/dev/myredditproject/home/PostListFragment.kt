package gonzalo.dev.myredditproject.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.activityViewModels
import gonzalo.dev.core.domain.model.Post
import gonzalo.dev.myredditproject.R
import gonzalo.dev.myredditproject.common.list.BaseListFragment
import gonzalo.dev.myredditproject.image.ImageActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi

class PostListFragment : BaseListFragment<Post>() {

    companion object {
        const val TAG = "fataPostListFrag"
        fun newInstance() = PostListFragment()
    }

    private val viewModel: HomeViewModel by activityViewModels()
    private val adapter = PostAdapter(this)

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i(TAG, "is dualpane:  ${viewModel.isDualPane}")

        viewModel.postsState.observe(viewLifecycleOwner, { dataSet ->
            dataSetReady()
            getList().adapter = adapter.also { it.setData(dataSet) }
        })

        viewModel.notifyState.observe(viewLifecycleOwner, {
            dataSetReady()
            adapter.notifyDataSetChanged()
        })

        viewModel.queryTextState.observe(viewLifecycleOwner, {
            adapter.filter.filter(it)
        })

        viewModel.readPosts()
    }

    @ExperimentalCoroutinesApi
    override fun onSwipe() {
        viewModel.refreshList()
    }

    override fun onItemClicked(item: Post) {
        Log.i(TAG, "onItemClicked: ")
        viewModel.showPostDetail(item)
    }

    override fun onItemAction(item: Post, view: View) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity as Activity, view, getString(R.string.thumb_transition)
        )

        startActivity(Intent(context, ImageActivity::class.java).apply {
            putExtra(ImageActivity.EXTRA, item.url)
        }, options.toBundle())
    }

    @ExperimentalCoroutinesApi
    override fun loadNextPage() {
        Log.i(TAG, "loadMoreItems: ")
        viewModel.loadMore(viewModel.feedData.after)
    }

}