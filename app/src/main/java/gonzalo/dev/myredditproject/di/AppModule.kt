package gonzalo.dev.myredditproject.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import gonzalo.dev.core.data.repository.PostRepository
import gonzalo.dev.core.usecase.GetTopPosts
import gonzalo.dev.myredditproject.BuildConfig
import gonzalo.dev.myredditproject.common.Timeout
import gonzalo.dev.myredditproject.home.PostsService
import gonzalo.dev.myredditproject.implementations.PostDataSourceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
@InstallIn(ActivityComponent::class)
class AppModule {

    @Provides
    fun provideHttpInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    fun provideOkHttpInstance(interceptor: HttpLoggingInterceptor, timeout: Timeout) =
        OkHttpClient.Builder()
            .connectTimeout(timeout.timeout, TimeUnit.SECONDS)
            .readTimeout(timeout.timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout.timeout, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()

    @Provides
    fun provideGsonInstance() = GsonBuilder()
        .enableComplexMapKeySerialization()
        .serializeNulls()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setPrettyPrinting()
        .setVersion(1.0)
        .create()

    @Provides
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(provideGsonInstance()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    fun providePostService(): PostsService {
        val timeOut = PostsService::class.java.getAnnotation(Timeout::class.java)
        return provideRetrofitInstance(provideOkHttpInstance(provideHttpInterceptor(), timeOut!!))
            .create(PostsService::class.java)
    }

    @Provides
    fun provideRepository() = PostRepository(PostDataSourceImpl(providePostService()))

    @Provides
    fun provideGetPostUseCase(): GetTopPosts = GetTopPosts(provideRepository())
}