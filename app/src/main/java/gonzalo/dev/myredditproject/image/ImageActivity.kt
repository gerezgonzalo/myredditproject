package gonzalo.dev.myredditproject.image

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import gonzalo.dev.myredditproject.R
import gonzalo.dev.myredditproject.common.mvvm.BaseActivity
import gonzalo.dev.myredditproject.common.mvvm.BaseViewModel
import gonzalo.dev.myredditproject.common.util.FileUtil
import gonzalo.dev.myredditproject.common.util.FrescoUtils
import gonzalo.dev.myredditproject.databinding.ActivityImageBinding

class ImageActivity : BaseActivity<BaseViewModel>() {
    companion object {
        const val EXTRA = "ia_extra"
        private const val WRITE_PERMISSION_REQUEST = 921
    }

    private val binding by lazy { ActivityImageBinding.inflate(layoutInflater) }
    private var imageUrl: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.hasExtra(EXTRA)) {
            imageUrl = intent.getStringExtra(EXTRA)
            FrescoUtils.setImage(binding.fullImage, imageUrl)
        }

    }

    override fun createViewModelFactory(): BaseViewModel {
        val vm: BaseViewModel by viewModels()
        return vm
    }

    override fun getRootView(): View {
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == WRITE_PERMISSION_REQUEST) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    FileUtil.download(applicationContext, imageUrl)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.image_viewer_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.image_download) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                FileUtil.download(applicationContext, imageUrl)
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    WRITE_PERMISSION_REQUEST
                )
            }
        }
        return true
    }
}