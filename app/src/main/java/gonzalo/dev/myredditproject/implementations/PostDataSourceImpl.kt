package gonzalo.dev.myredditproject.implementations

import android.util.Log
import gonzalo.dev.core.data.dto.TopResponse
import gonzalo.dev.core.data.repository.PostDataSource
import gonzalo.dev.core.domain.model.Post
import gonzalo.dev.myredditproject.home.PostsService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class PostDataSourceImpl @Inject constructor(private val postService: PostsService) :
    PostDataSource {
    companion object {
        private val TAG = "fataPostDataSourceImpl"
    }

    /**
     * Fetch from the remote server
     */
    @ExperimentalCoroutinesApi
    override suspend fun fetchPosts(nextPage: String?): Flow<TopResponse> {
        Log.i(TAG, "fetchPosts: ")
        return flow {
            val response = postService.fetchPosts(nextPage ?: "")
            emit(response)
        }.flowOn(Dispatchers.IO)
    }

    /**
     * Read from local database.
     */
    override suspend fun readPosts(): List<Post> {
        //Won't implement this method.
        throw IllegalStateException("Method not implemented.")
    }
}