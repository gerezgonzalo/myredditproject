package gonzalo.dev.myredditproject.common.util

import android.app.DownloadManager
import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.core.net.toUri
import gonzalo.dev.myredditproject.R

object FileUtil {
    private const val TAG = "fataFileUti"

    /**
     * Download an image from the cloud.
     *
     * @param applicationContext The application context.
     * @param imageUrl The file url.
     */
    fun download(applicationContext: Context, imageUrl: String?) {
        if (imageUrl == null) {
            Log.e(TAG, "The url can not to be null.")
            return
        }
        Log.i(TAG, "download: $imageUrl")
        val request = DownloadManager.Request(imageUrl.toUri())
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            .setDestinationInExternalPublicDir(
                Environment.DIRECTORY_PICTURES,
                Utils.getSystemDate()
            )
            .setTitle(applicationContext.getString(R.string.downloading))
            .setDescription(applicationContext.getString(R.string.downloading_disclaimer))
            .setAllowedOverMetered(true)
            .setAllowedOverRoaming(true)

        val downloadMgr =
            applicationContext.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadMgr.enqueue(request)
    }
}