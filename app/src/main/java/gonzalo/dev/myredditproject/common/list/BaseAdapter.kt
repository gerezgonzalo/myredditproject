package gonzalo.dev.myredditproject.common.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView

/**
 * @param T The data set type data.
 * @param VH The ViewHolder type data.
 */
abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>(),
    Filterable {

    private val dataSetCopy = ArrayList<T>()

    private var itemFilter: ItemFiler? = null
    protected var _data = ArrayList<T>()
    var canClick = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context).inflate(getItemResource(), parent, false)
        return getViewHolderInstance(view)
    }

    override fun getItemCount(): Int {
        return _data.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        onBind(holder, position)
    }

    abstract fun setData(dataSet: ArrayList<T>)

    abstract fun getItemResource(): Int

    abstract fun getViewHolderInstance(view: View): VH

    abstract fun onBind(holder: VH, position: Int)

    override fun getFilter(): Filter {
        return itemFilter ?: ItemFiler()
    }

    inner class ItemFiler() : Filter() {
        init {
            if (dataSetCopy.isEmpty()) dataSetCopy.addAll(_data)
        }

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            if (constraint?.length == 0) {
                return FilterResults().apply {
                    values = dataSetCopy
                    count = dataSetCopy.size
                }
            }

            val filterObject = doPerformFiltering(constraint, dataSetCopy)
            return FilterResults().apply {
                values = filterObject?.values
                count = filterObject?.count ?: 0
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            _data.clear()
            _data.addAll(results?.values as ArrayList<T>)
            notifyDataSetChanged()
        }
    }

    /**
     * Method to perform filter data in the current data set called "data".
     *
     * @param constraint The char sequence inputted by the user.
     * @return Return an object type FilterObject in order to be able to fill the needed data in the
     * ItemFiler class.
     */
    abstract fun doPerformFiltering(
        constraint: CharSequence?,
        originalDataSet: ArrayList<T>
    ): FilterObject?

    /**
     * Class used as template in order to be able to fill the the FilterResults object in the
     * ItemFilter class.
     *
     * @param values The resulted array after filter.
     * @param count The resultad array size.
     */
    data class FilterObject(val values: Any, val count: Int)
}