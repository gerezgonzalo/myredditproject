plugins {
    id(Plugins.javaLibrary)
    id(Plugins.kotlin)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(Dependencies.kotlinStdlib)
    implementation(Dependencies.kotlinxCoroutines)
    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.mockitoCore)
    testImplementation(Dependencies.Test.kotlinCoroutines)
    testImplementation(Dependencies.Test.mockk)
}