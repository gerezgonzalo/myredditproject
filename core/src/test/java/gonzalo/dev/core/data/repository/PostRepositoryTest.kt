package gonzalo.dev.core.data.repository

import gonzalo.dev.core.common.AbstractTest
import gonzalo.dev.core.data.dto.TopResponse
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

class PostRepositoryTest : AbstractTest() {

    @Test
    fun fetchTopPosts() {
        runBlocking {
            val flow: Flow<TopResponse> = mockk(relaxed = true)
            val postDataSource: PostDataSource = mockk(relaxed = true)
            val repo = PostRepository(postDataSource)

            coEvery { repo.fetch(any()) } returns flow

            repo.fetch("")

            coVerify {
                postDataSource.fetchPosts(any())
            }
        }

    }
}