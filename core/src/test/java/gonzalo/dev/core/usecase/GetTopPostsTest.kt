package gonzalo.dev.core.usecase

import gonzalo.dev.core.common.AbstractTest
import gonzalo.dev.core.data.dto.TopResponse
import gonzalo.dev.core.data.repository.PostRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

class GetTopPostsTest : AbstractTest() {

    @Test
    fun `Given a valid repository, invoke the top post use case`() {
        val flow: Flow<TopResponse> = mockk(relaxed = true)
        val postRepository: PostRepository = mockk(relaxed = true)
        val getTopPosts = GetTopPosts(postRepository)

        runBlocking {
            coEvery { getTopPosts("") } returns flow

            getTopPosts("")

            coVerify {
                postRepository.fetch("")
            }
        }
    }
}