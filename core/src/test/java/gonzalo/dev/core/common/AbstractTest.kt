package gonzalo.dev.core.common

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
abstract class AbstractTest {

    @Before
    fun setUp(){
        //Do something.
    }
}