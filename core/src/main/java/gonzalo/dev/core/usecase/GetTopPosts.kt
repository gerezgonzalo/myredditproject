package gonzalo.dev.core.usecase

import gonzalo.dev.core.data.repository.PostRepository

class GetTopPosts(private val repository: PostRepository) {

    suspend operator fun invoke(nextPage: String? = "") = repository.fetch(nextPage)
}