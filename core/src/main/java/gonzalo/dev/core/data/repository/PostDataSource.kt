package gonzalo.dev.core.data.repository

import gonzalo.dev.core.data.dto.TopResponse
import gonzalo.dev.core.domain.model.Post
import kotlinx.coroutines.flow.Flow

interface PostDataSource {

    suspend fun fetchPosts(nextPage: String?): Flow<TopResponse>

    suspend fun readPosts(): List<Post>
}