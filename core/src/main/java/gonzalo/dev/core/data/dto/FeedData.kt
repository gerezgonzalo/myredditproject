package gonzalo.dev.core.data.dto

data class FeedData(
    val modhash: String,
    val dist: Int,
    val children: List<Children>,
    val after: String,
    val before: String?
)