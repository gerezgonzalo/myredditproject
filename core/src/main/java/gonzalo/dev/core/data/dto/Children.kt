package gonzalo.dev.core.data.dto

import gonzalo.dev.core.domain.model.Post

data class Children(
    val kind: String,
    val data: Post
)