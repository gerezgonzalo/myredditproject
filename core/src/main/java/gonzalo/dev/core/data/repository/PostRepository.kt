package gonzalo.dev.core.data.repository

class PostRepository(private val postDataSource: PostDataSource) {

    /**
     * Fetch data from the remote server.
     */
    suspend fun fetch(nextPage: String?) = postDataSource.fetchPosts(nextPage)

    /**
     * Read data from the local database.
     */
    suspend fun read() = postDataSource.readPosts()
}