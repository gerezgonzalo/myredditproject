package gonzalo.dev.core.domain.model

import java.io.Serializable

/**
 * The data containing the post attributes.
 */
data class Post(
    val author: String,
    var clicked: Boolean,
    val title: String,
    val name: String,
    val createdUtc: Long,
    val thumbnail: String,
    val numComments: Int,
    val url: String,
) : Serializable
// FIXME: 18/11/2020 We must use the Parcelable interface and annotate this class with @Parcelize
// FIXME: 18/11/2020 but I'm getting unresolve reference error. So we use Serializable as workaround.

{
    fun contains(value: String): Boolean {
        return author.contains(value, ignoreCase = true) || title.contains(value, ignoreCase = true)
                || name.contains(value, ignoreCase = true)
    }
}